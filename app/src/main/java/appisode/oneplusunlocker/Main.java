package appisode.oneplusunlocker;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.stericson.RootShell.RootShell;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Main extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {
    Switch mTamperSwitch;
    Switch mUnlockedSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTamperSwitch = (Switch)findViewById(R.id.switch_tamper);
        mUnlockedSwitch = (Switch)findViewById(R.id.switch_unlocked);

        if(RootShell.isAccessGiven()) {
            try {
                mTamperSwitch.setChecked(readByteFromCommand("dd ibs=1 count=1 skip=1048084 if=/dev/block/platform/msm_sdcc.1/by-name/aboot") == 1);
                mUnlockedSwitch.setChecked(readByteFromCommand("dd ibs=1 count=1 skip=1048080 if=/dev/block/platform/msm_sdcc.1/by-name/aboot") == 1);
            } catch (IOException e) {
                showError(R.string.error_reading);
            }
            mTamperSwitch.setOnCheckedChangeListener(this);
            mUnlockedSwitch.setOnCheckedChangeListener(this);
        }
        else {
            showError(R.string.error_no_root);
        }
    }

    void showError(CharSequence error) {
        findViewById(R.id.controls).setVisibility(View.GONE);
        ((TextView)findViewById(R.id.text_error)).setText(error);
    }

    void showError(int errorRes) {
        showError(getText(errorRes));
    }

    @Override
    public void onCheckedChanged(final CompoundButton buttonView, final boolean isChecked) {
        new AsyncTask<Void, Void, Boolean>() {
            ProgressDialog mProgressDialog;

            @Override
            protected void onPreExecute() {
                mProgressDialog = ProgressDialog.show(Main.this, getText(R.string.writing), getText(R.string.please_wait));
            }
            @Override
            protected Boolean doInBackground(Void... params) {
                try {
                    String tempFile = getApplicationInfo().dataDir + "/oneplusunlockertemp";
                    Process p = Runtime.getRuntime().exec("su");
                    DataOutputStream w = new DataOutputStream(p.getOutputStream());
                    w.writeBytes("echo -n -e \\\\x" + Integer.toHexString(isChecked ? 1 : 0) + " > " + tempFile + "\n");
                    w.flush();
                    w.writeBytes("dd obs=1 count=1 seek=" + (buttonView == mTamperSwitch ? 1048084 : 1048080) + " if=" + tempFile + " of=/dev/block/platform/msm_sdcc.1/by-name/aboot" + "\n");
                    w.flush();
                    w.writeBytes("rm " + tempFile + "\n");
                    w.flush();
                    w.writeBytes("exit\n");
                    w.flush();
                    try {
                        p.waitFor();
                    } catch (InterruptedException e) {}
                } catch (IOException e) {
                    return false;
                }
                return true;
            }

            @Override
            protected void onPostExecute(Boolean result) {
                mProgressDialog.dismiss();
                if(!result) {
                    showError(R.string.error_writing);
                }
            }
        }.execute();
    }

    public int readByteFromCommand(String theCommand) throws IOException {
        Process p = Runtime.getRuntime().exec("su");
        DataOutputStream w = new DataOutputStream(p.getOutputStream());
        DataInputStream r = new DataInputStream(p.getInputStream());

        w.writeBytes(theCommand + "\n");
        w.flush();
        int resultByte = r.readByte();
        w.writeBytes("exit\n");
        w.flush();
        try {
            p.waitFor();
        } catch (InterruptedException e) {}
        return resultByte;
    }
}
